You can use these commands to run the app:

javac -cp json-simple-1.1.1.jar Assignment.java

java -cp json-simple-1.1.1.jar Assignment.java <data file path> <definition file path>
package com.company;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class Assignment {

    public static void main(String[] args) {
        String dataFilePath = args[0];
        String definitionFilePath = args[1];
        JSONParser parser = new JSONParser();

        try {
            Object dataFile = parser.parse(new FileReader(dataFilePath));
            Object definitionFile = parser.parse(new FileReader(definitionFilePath));
            JSONArray dataValues = (JSONArray) dataFile;
            JSONObject definitionValues = (JSONObject) definitionFile;

            Long topPerformersThreshold = (Long) definitionValues.get("topPerformersThreshold");
            Boolean useExperienceMultiplier = (Boolean) definitionValues.get("useExperienceMultiplier");
            Long periodLimit = (Long) definitionValues.get("periodLimit");
            Map<String, Double> results = new HashMap<>();

            for (Object value : dataValues) {
                JSONObject person = (JSONObject) value;
                String name = (String) person.get("name");
                Long salesPeriod = (Long) person.get("salesPeriod");
                Long totalSales = (Long) person.get("totalSales");
                Double experienceMultiplier = (Double) person.get("experienceMultiplier");
                double score;

                if (useExperienceMultiplier) {
                    score = totalSales / salesPeriod * experienceMultiplier;
                } else {
                    score = totalSales / salesPeriod;
                }

                if (salesPeriod <= periodLimit) {
                    results.put(name, score);
                }
            }
            FileWriter writer = new FileWriter("result.csv");
            writer.append("Name");
            writer.append(",");
            writer.append("Score\n");

            Long scoreLimit = results.size() / 100 * topPerformersThreshold;
            List<Double> scores = results.values()
                    .stream()
                    .sorted(Comparator.reverseOrder())
                    .collect(Collectors.toList());
            Double minScore = scores.get(Math.toIntExact(scoreLimit));

            for (Double score : scores) {
                if (score >= minScore) {
                    String name = results.keySet()
                            .stream()
                            .filter(key -> score.equals(results.get(key)))
                            .findFirst().get();
                    writer.append(name + ",");
                    writer.append(score.toString() + "\n");
                    results.remove(name);
                }
            }
            writer.flush();
            writer.close();
        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }
    }
}
